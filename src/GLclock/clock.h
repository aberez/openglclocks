#ifndef CLOCK_H
#define CLOCK_H

//#pragma comment(lib, "libSOIL.lib") 
#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include <time.h>
#include <gl/glut.h>
#include "Model_3DS.h"
#include "GLTexture.h"
//#include "SOIL.h"


//#define MYDEBUG
#ifdef MYDEBUG
static void checkErrors( void ) 
{ 
  GLenum error;

  while ( (error = glGetError()) != GL_NO_ERROR ) { 
      fprintf( stderr, "Error: %s\n", (char *) gluErrorString( error ) ); 
  } 
}
#else
static void checkErrors( void ) 
{
}
#endif


#define	BUFSIZE		256     // select buffer will be 256 ints in size
#define CUBE_NAME       0
#define X_PICK_SIZE     10       // pick region is 5 x 5 pixels in size
#define Y_PICK_SIZE     10  
#define PENDULUM 6


struct Camera{
	float eyex,eyey,eyez;
	float cenx,ceny,cenz;
	float upx,upy,upz;
	float speed;
	void move(int pos){
		speed=0.1f;
		float delx=cenx-eyex;
		float dely=ceny-eyey;
		float delz=cenz-eyez;

		float magnitude=sqrt(delx*delx+dely*dely+delz*delz);
		float norx=delx/magnitude;
		float nory=dely/magnitude;
		float norz=delz/magnitude;

		
		eyex+=pos*norx*speed;
		eyey+=pos*nory*speed;
		eyez+=pos*norz*speed;

		cenx+=pos*norx*speed;
		ceny+=pos*nory*speed;
		cenz+=pos*norz*speed;
	}
} Cam;


void InitLights();
void ReshapeWindow(int w, int h);
void SetCamera();
void shadowMatrix(GLfloat shadowMat[4][4], GLfloat groundplane[4], GLfloat lightpos[4]);
void Init();
void drawFloor(void);
void DrawSecondHand();
void DrawClock();
void DrawScene();
void Display();
void IdleRedraw();
void ProcessNormalKeys(unsigned char key, int x, int y) ;
void PressKey(int key, int xx, int yy);
void Mouse(int button, int state, int x, int y);
void processHits (GLint hits, GLuint buffer[]);
static void checkHits( int x, int y );
GLuint LoadTextureRAW( const char * filename, int width, int height, int wrap );
void _tbPointToVector(int x, int y, int width, int height, float v[3]);
void _tbAnimate(void);
void tbInit();
void tbMatrix();
void tbReshape(int width, int height);
void tbStartMotion(int x, int y, int button, int time);
void tbStopMotion(int button, unsigned time);
void tbMotion(int x, int y);



#endif CLOCK_H




