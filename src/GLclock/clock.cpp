#include "clock.h"


float ClockRotateDelta=0.4;//Little rotate angle - good quality bad performance
int WaitTime = 5; //wait time good performance bad quality


GLfloat lightZeroPosition[] = {6.0, 10.0, 10.0, 0.0};
GLfloat lightZeroColor[] = {0.99, 0.99, 0.99, 1.0}; 
GLfloat lightOnePosition[] = {-4.0, 5.0, 4.0, 1.0};
GLfloat lightOneColor[] = {0.50, 0.27 ,0.11 , 1.0};
GLfloat light_ambient[] = {0.2, 0.2, 0.2, 1.0};
GLfloat light_diffuse[] =  {0.8, 0.8, 0.8, 1.0};
GLfloat light_specular[] = {0.8, 0.8, 0.8, 1.0};

GLTexture floortex;
GLuint texture;

enum {
  MISSING, EXTENSION, ONE_DOT_ONE
};
enum {
  X, Y, Z, W
};
enum {
  A, B, C, D
};

int polygonOffsetVersion;
static int forceExtension = 0;
static GLfloat floorShadow[4][4];
static GLfloat floorPlane[4]={0,10,0,0};

const double clockRootR=6.88;
float ClockRotateAngle=0;
bool isClockRotate=true;
bool isPendulumRotate=false;


float pendulumAngle=0;
float pendulumAnglerad=0;
int pendulumDir=1;
const float deltaPendulum=0.04;
const float pendulumAngleMax=8;

struct tm *newtime;
time_t ltime;

Model_3DS clockModel;

	



void ReshapeWindow(int w, int h) {	
	if(h == 0) h = 1;
	float ratio = 1.0* w / h;
	tbReshape(w, h);
	// Use the Projection Matrix
	glMatrixMode(GL_PROJECTION);

	glLoadIdentity();
	// Set the viewport to be the entire window
	glViewport(0, 0, (GLsizei) w,(GLsizei) h);
	// Set the correct perspective.
	gluPerspective(50,ratio,1,100);//!!!
	// Get Back to the Modelview
	glMatrixMode(GL_MODELVIEW);
}

void SetCamera(){
	gluLookAt(Cam.eyex,Cam.eyey,Cam.eyez,Cam.cenx,Cam.ceny,Cam.cenz,Cam.upx,Cam.upy,Cam.upz);	
}








void Init(){

	tbInit();
	glClearColor(0.9, 0.9, 0.9, 1.0);
	glShadeModel (GL_SMOOTH);
	glEnable(GL_LINE_SMOOTH);
	glEnable(GL_CULL_FACE);
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_NORMALIZE);
	InitLights();
	
	  /* Setup the view of the cube. */
	 glMatrixMode(GL_PROJECTION);
	 gluPerspective( /* field of view in degree */ 50.0,
					/* aspect ratio */ 1.0,
					/* Z near */1.0, 
					/* Z far */ 100.0);
	 glMatrixMode(GL_MODELVIEW);


	Cam.eyex=0;
	Cam.eyey=10;
	Cam.eyez=7.0f;
	Cam.upx=0;
	Cam.upy=1.0f;
	Cam.upz=0;
	Cam.cenx=0;
	Cam.ceny=5;
	Cam.cenz=0;

	SetCamera();
	clockModel.Load("mainclock.3ds"); // Load the model
	 //texture = LoadTextureRAW( "floor.raw", 512, 512, TRUE ); //load texture using example
	 //floortex.Load("floor.bmp"); //load texture using gltexture
	checkErrors();
}

void InitLights(){//ste light0 and light1

  glLightfv(GL_LIGHT0, GL_AMBIENT, light_ambient);
  glLightfv(GL_LIGHT0, GL_DIFFUSE, light_diffuse);
  glLightfv(GL_LIGHT0, GL_SPECULAR, light_specular);
  //glLightfv(GL_LIGHT0, GL_POSITION, light_position);

  glEnable(GL_LIGHT0);

  glEnable(GL_LIGHTING);
  glLightfv(GL_LIGHT1, GL_AMBIENT, light_ambient);
  glLightfv(GL_LIGHT1, GL_DIFFUSE, light_diffuse);
  glLightfv(GL_LIGHT1, GL_SPECULAR, light_specular);
  glLightModeli(GL_LIGHT_MODEL_LOCAL_VIEWER, 1);


   glLightModeli(GL_LIGHT_MODEL_LOCAL_VIEWER, 1);
  glLightf(GL_LIGHT0, GL_CONSTANT_ATTENUATION, 0.1);
  glLightf(GL_LIGHT0, GL_LINEAR_ATTENUATION, 0.05);

  glEnable(GL_LIGHT0);
  glEnable(GL_LIGHT1);

   glLightfv(GL_LIGHT0, GL_POSITION, lightZeroPosition);
   glLightfv(GL_LIGHT1, GL_POSITION, lightOnePosition);
}



void drawFloor(void)
{
  glDisable(GL_LIGHTING);
  glBegin(GL_QUADS);
    glVertex3f(-18.0, 0.0, 27.0);
    glVertex3f(27.0, 0.0, 27.0);
    glVertex3f(27.0, 0.0, -18.0);
    glVertex3f(-18.0, 0.0, -18.0);
  glEnd();
  glEnable(GL_LIGHTING);
}

//void getFloorTex(){ //load texture using SOIL .done
//	GLuint tex_2d = SOIL_load_OGL_texture(
//		"tex.bmp",
//		SOIL_LOAD_AUTO,
//		SOIL_CREATE_NEW_ID,
//		SOIL_FLAG_MIPMAPS | SOIL_FLAG_NTSC_SAFE_RGB | SOIL_FLAG_COMPRESS_TO_DXT
//	);
//	if( 0 == tex_2d ){
//	printf( "SOIL loading error: '%s'\n", SOIL_last_result() );
//	}
//	glBindTexture( GL_TEXTURE_2D, tex_2d );
//	glTexEnvf( GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE );
//	glTexParameterf( GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT );
//	glTexParameterf( GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT );
//}

void DrawSecondHand(){
		/*brass material
		GLfloat ambient[] = {0.33, 0.22, 0.03, 1.0}, 
		diffuse[] = {0.78, 0.57, 0.11, 1.0},
		specular[] = {0.99, 0.91, 0.81, 1.0}, 
		shininess = 27.8;*/
		GLfloat ambient[] = {0.01, 0.01, 0.01, 1.0}, 
		diffuse[] = {0.01, 0.01, 0.01, 1.0}, 
		specular[] = {0.01, 0.01, 0.01, 1.0},  
		shininess = 27.8;

		GLfloat n_ambient[] = {0.3, 0.3, 0.3, 0.3},
		n_diffuse[] = {0.3, 0.3, 0.3, 0.3},
		n_specular[] = {0.3, 0.3, 0.3, 0.3}, 
		n_shininess = 70.8;
		

		glMaterialfv(GL_FRONT, GL_AMBIENT, ambient);
		glMaterialfv(GL_FRONT, GL_DIFFUSE, diffuse);
		glMaterialfv(GL_FRONT, GL_SPECULAR, specular);
		glMaterialf(GL_FRONT, GL_SHININESS, shininess);

		glLineWidth(0.5);
		glPushMatrix();
			checkErrors();
			double alpha=(360/60) * newtime->tm_sec;
			double alpharad=alpha*3.14159265/180;
			glTranslatef(0.0,clockRootR,0.0);
			glRotatef(-alpha, 0.0f, 0.0f, 1.0f);
			glColor4f(0.1,0.1,0.1,1.0);
			glBegin(GL_LINES);			 
				 glVertex3f(0.0, 0.0, 0.85);
				 glVertex3f(0.0, 0.5, 0.85);
			glEnd();


		 glMaterialfv(GL_FRONT, GL_AMBIENT, n_ambient);
		 glMaterialfv(GL_FRONT, GL_DIFFUSE, n_diffuse);
		 glMaterialfv(GL_FRONT, GL_SPECULAR, n_specular);
		 glMaterialf(GL_FRONT, GL_SHININESS, n_shininess);
	
		glPopMatrix();
}



void DrawClock(){	
	
	glPushMatrix();
		glRotatef(ClockRotateAngle, 0.0f, 1.0f, 0.0f); //rotate if animated
		glTranslatef(0.0,0.5,0.0);
		checkErrors();
		//draw mintute hand
		double alpha=(360.0/60) * newtime->tm_min;
		double alpharad=alpha*3.14159265/180;
		clockModel.Objects[5].rot.z=-alpha;
		clockModel.Objects[5].pos.x=-clockRootR*sin(alpharad);
		clockModel.Objects[5].pos.y=-clockRootR*(1-cos(alpharad));

		//draw hour hand
		 alpha=(360/12) * newtime->tm_hour  + (360/60) * ((newtime->tm_min)/12.0);
		 alpharad=alpha*3.14159265/180;
		 clockModel.Objects[4].rot.z=-alpha;
		 clockModel.Objects[4].pos.x=-clockRootR*sin(alpharad);
		 clockModel.Objects[4].pos.y=-clockRootR*(1-cos(alpharad));

		//draw pendulum
		 if (isPendulumRotate){
			pendulumAngle+=pendulumDir*deltaPendulum;
			if (abs(pendulumAngle)>pendulumAngleMax) pendulumDir*=-1;
		 }
		 pendulumAnglerad=pendulumAngle*3.14159265/180;
		 clockModel.Objects[6].rot.z=-pendulumAngle;
		 clockModel.Objects[6].pos.x=-clockRootR*sin(pendulumAnglerad);
		 clockModel.Objects[6].pos.y=-clockRootR*(1-cos(pendulumAnglerad));


		 
		clockModel.Draw();
		DrawSecondHand();
	glPopMatrix();	
	
}



void DrawScene(){//draw main world whith clocks and floor
	glLoadIdentity();
	SetCamera();
	shadowMatrix(floorShadow, floorPlane, lightZeroPosition);
	
	glPushMatrix();
	{

	   	/* Perform scene rotations based on user mouse input. */
			tbMatrix();
		  /* Don't update color or depth. */
		  glDisable(GL_DEPTH_TEST);
		  glColorMask(GL_FALSE, GL_FALSE, GL_FALSE, GL_FALSE);

		  /* Draw 1 into the stencil buffer. */
		  glEnable(GL_STENCIL_TEST);
		  glStencilOp(GL_REPLACE, GL_REPLACE, GL_REPLACE);
		  glStencilFunc(GL_ALWAYS, 1, 0xffffffff);
	
		   /* Now render floor; floor pixels just get their stencil set to 1. */
		  drawFloor();
	  
			/* Re-enable update of color and depth. */ 
		  glColorMask(GL_TRUE, GL_TRUE, GL_TRUE, GL_TRUE);
		  glEnable(GL_DEPTH_TEST);

		  /* Now, only render where stencil is set to 1. */
		  glStencilFunc(GL_EQUAL, 1, 0xffffffff);  /* draw if ==1 */
		  glStencilOp(GL_KEEP, GL_KEEP, GL_KEEP);
		//end if sentil
		  glPushMatrix();
		  {
			  /* The critical reflection step: Reflect dinosaur through the floor     (the Y=0 plane) to make a relection. */
				glScalef(1.0, -1.0, 1.0);
				 /* Position lights now in reflected space. */
				  glLightfv(GL_LIGHT0, GL_POSITION, lightZeroPosition);
				  glLightfv(GL_LIGHT1, GL_POSITION, lightOnePosition);

				
				  glEnable(GL_NORMALIZE);
				  glCullFace(GL_FRONT);
				  /* Draw the reflected dinosaur. */
				  DrawClock();

				  /* Disable noramlize again and re-enable back face culling. */
				  glDisable(GL_NORMALIZE);
				  glCullFace(GL_BACK);
		  }
		  glPopMatrix();

		   glLightfv(GL_LIGHT0, GL_POSITION, lightZeroPosition);
		   glLightfv(GL_LIGHT1, GL_POSITION, lightOnePosition);
		   
		   glDisable(GL_STENCIL_TEST);
		   

			/* Draw "top" of floor.  Use blending to blend in reflection. */
			glEnable(GL_BLEND);
			glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
			glColor4f(0.9, 0.9, 0.9, 0.3);
			drawFloor();
			glDisable(GL_BLEND);

			/* Draw "bottom" of floor in gray. */
			glFrontFace(GL_CW);  /* Switch face orientation. */
			glBindTexture( GL_TEXTURE_2D, texture );
			glColor4f(0.9, 0.9, 0.9, 1.0);
			drawFloor();
			glFrontFace(GL_CCW);

			/* Draw "actual" clock*/
			DrawClock();


			/* Draw Shadow*/
	    glStencilFunc(GL_LESS, 2, 0xffffffff);  /* draw if ==1 */
        glStencilOp(GL_REPLACE, GL_REPLACE, GL_REPLACE);
			switch (polygonOffsetVersion) {
			case EXTENSION:
			#ifdef GL_EXT_polygon_offset
			  glEnable(GL_POLYGON_OFFSET_EXT);
			 break;
			#endif
			#ifdef GL_VERSION_1_1
			case ONE_DOT_ONE:
			  glEnable(GL_POLYGON_OFFSET_FILL);
				break;
			#endif
			case MISSING:
			  
			 break;
			}
		 
		  glEnable(GL_BLEND);
		  glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
		  glDisable(GL_LIGHTING); 
		  glColor4f(0.0, 0.0, 0.0, 1.0);

		  glPushMatrix();
		   /* Project the shadow. */
			glMultMatrixf((GLfloat *) floorShadow);
			DrawClock();
		  glPopMatrix();

		  glDisable(GL_BLEND);
		  glEnable(GL_LIGHTING);

		  switch (polygonOffsetVersion) {
			#ifdef GL_EXT_polygon_offset
				case EXTENSION:
				  glDisable(GL_POLYGON_OFFSET_EXT);
				  break;
			#endif
			#ifdef GL_VERSION_1_1
				case ONE_DOT_ONE:
					  glDisable(GL_POLYGON_OFFSET_FILL);
				  break;
			#endif
				case MISSING:
				
				  break;
			}
		  glDisable(GL_STENCIL_TEST);

	}
	glPopMatrix();	

}



void Display(){

	time(&ltime); // Get time
	newtime = localtime(&ltime); // Convert to local time 
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT); 
	DrawScene();// Renders the model to the screen
	checkErrors();
	glutSwapBuffers();
}

void IdleRedraw(){
	static int time=glutGet(GLUT_ELAPSED_TIME);
	if (glutGet(GLUT_ELAPSED_TIME)-time>WaitTime){//machine independet rotion
		time=glutGet(GLUT_ELAPSED_TIME);
		if (isClockRotate) ClockRotateAngle+=ClockRotateDelta;
		checkErrors();
		glutPostRedisplay();
	}
}


void ProcessNormalKeys(unsigned char key, int x, int y) {
 switch (key) {
  case 27: exit(0); break;//ESC
  case 32:  isClockRotate=!isClockRotate; break;//spacebar
  case '2': if (WaitTime>1) WaitTime--; break;
  case '1': if (WaitTime<10) WaitTime++; break;
  case '8':  if (ClockRotateDelta>0.15) ClockRotateDelta-=0.05; break;
  case '9':  if (ClockRotateDelta<0.8) ClockRotateDelta+=0.05; break;

 }
}

void PressKey(int key, int xx, int yy) {
       switch (key) {
	   case GLUT_KEY_UP : Cam.move(1); break;
       case GLUT_KEY_DOWN : Cam.move(-1);  break;
       }
} 


void Mouse(int button, int state, int x, int y)
{
	switch( button ){
	case GLUT_LEFT_BUTTON:
		if (state == GLUT_DOWN)
			tbStartMotion(x, y, button, glutGet(GLUT_ELAPSED_TIME));
			else if (state == GLUT_UP)
			 tbStopMotion(button, glutGet(GLUT_ELAPSED_TIME));
		break;
	case GLUT_RIGHT_BUTTON:
		if( state == GLUT_DOWN )
			checkHits( x, y );
	    break;
	}
}



void Motion(int x, int y)
{	
	tbMotion(x, y);
}



static int
supportsOneDotOne(void)
{
  const char *version;
  int major, minor;

  version = (char *) glGetString(GL_VERSION);
  if (sscanf(version, "%d.%d", &major, &minor) == 2)
    return major >= 1 && minor >= 1;
  return 0;            /* OpenGL version string malformed! */
}


int main(int argc, char **argv)
{

  glutInit(&argc, argv);

   glutInitDisplayMode(GLUT_RGB | GLUT_DOUBLE | GLUT_DEPTH | GLUT_STENCIL);
  glutInitWindowPosition(100,100);
  glutInitWindowSize(640,480);
  glutCreateWindow("3D Clock");


  #ifdef GL_VERSION_1_1
  if (supportsOneDotOne() && !forceExtension) {
    polygonOffsetVersion = ONE_DOT_ONE;
    glPolygonOffset(-2.0, -1.0);
  } else
#endif
  {
#ifdef GL_EXT_polygon_offset
  /* check for the polygon offset extension */
  if (glutExtensionSupported("GL_EXT_polygon_offset")) {
    polygonOffsetVersion = EXTENSION;
    glPolygonOffsetEXT(-0.1, -0.002);
  } else 
#endif
    {
      polygonOffsetVersion = MISSING;
      printf("\ndinoshine: Missing polygon offset.\n");
      printf("           Expect shadow depth aliasing artifacts.\n\n");
    }
  }
  
  
  // register callbacks  
  glutDisplayFunc(Display);
  glutKeyboardFunc(ProcessNormalKeys);
  glutSpecialFunc(PressKey);
  glutMouseFunc(Mouse);
  glutMotionFunc(Motion);
  glutReshapeFunc(ReshapeWindow);
  glutIdleFunc(IdleRedraw);
  

  Init();
	 GLfloat FogCol[4]={0.7f,0.7f,0.7f,1.0}; 
	 glEnable(GL_FOG);
	 glFogi(GL_FOG_MODE, GL_LINEAR);
	 glFogf(GL_FOG_START, 8.f);
	 glFogf(GL_FOG_END, 30.f);
	 glFogf(GL_FOG_DENSITY, 0.06);
	 glFogfv(GL_FOG_COLOR,FogCol);
	
  glutMainLoop();
  return 0;
}




/////////////////////////////////////////////////////////////////////////

static void checkHits( int x, int y )
{
  GLuint selectBuf[BUFSIZE];
  GLint hits;
  GLint viewport[4];
  GLdouble matrix[16];

  glGetIntegerv( GL_VIEWPORT, viewport );  // get viewport coordinates

  glSelectBuffer( BUFSIZE, selectBuf );
  glRenderMode( GL_SELECT );
  glInitNames();

  glMatrixMode( GL_PROJECTION );
  glPushMatrix();
    //setupCameraView();

    glMatrixMode( GL_PROJECTION );
    glGetDoublev( GL_PROJECTION_MATRIX, matrix );

    glLoadIdentity();
    gluPickMatrix( (double)x, (double)( viewport[3] - y ), 
		  X_PICK_SIZE, Y_PICK_SIZE, viewport );
             // creates picking region near cursor location
    glMultMatrixd( matrix );
	
	glMatrixMode( GL_MODELVIEW );
    DrawScene();

  glMatrixMode( GL_PROJECTION );
  glPopMatrix();

  hits = glRenderMode( GL_RENDER );
  processHits( hits, selectBuf );
  glMatrixMode( GL_MODELVIEW );
}					// End function checkHit

void processHits (GLint hits, GLuint buffer[])
{
   unsigned int i, j;
   GLuint names, *ptr, minZ,*ptrNames, numberOfNames;

   ptr = (GLuint *) buffer;
   minZ = 0xffffffff;
   for (i = 0; i < hits; i++) {	
      names = *ptr;
	  ptr++;
	  if (*ptr < minZ) {
		  numberOfNames = names;
		  minZ = *ptr;
		  ptrNames = ptr+2;
	  }
	  
	  ptr += names+2;
	}
  ptr = ptrNames;
  for (j = 0; j < numberOfNames; j++,ptr++) {
     if (*ptr>=0 && *ptr<8){
		 ///DO SMTHING;
		 isPendulumRotate=!isPendulumRotate;
	 }
  }
}




void shadowMatrix(GLfloat shadowMat[4][4],  GLfloat groundplane[4],  GLfloat lightpos[4])
{/* Create a matrix that will project the desired shadow. */
  GLfloat dot;

  /* Find dot product between light position vector and ground plane normal. */
  dot = groundplane[X] * lightpos[X] +
    groundplane[Y] * lightpos[Y] +
    groundplane[Z] * lightpos[Z] +
    groundplane[W] * lightpos[W];

  shadowMat[0][0] = dot - lightpos[X] * groundplane[X];
  shadowMat[1][0] = 0.f - lightpos[X] * groundplane[Y];
  shadowMat[2][0] = 0.f - lightpos[X] * groundplane[Z];
  shadowMat[3][0] = 0.f - lightpos[X] * groundplane[W];

  shadowMat[X][1] = 0.f - lightpos[Y] * groundplane[X];
  shadowMat[1][1] = dot - lightpos[Y] * groundplane[Y];
  shadowMat[2][1] = 0.f - lightpos[Y] * groundplane[Z];
  shadowMat[3][1] = 0.f - lightpos[Y] * groundplane[W];

  shadowMat[X][2] = 0.f - lightpos[Z] * groundplane[X];
  shadowMat[1][2] = 0.f - lightpos[Z] * groundplane[Y];
  shadowMat[2][2] = dot - lightpos[Z] * groundplane[Z];
  shadowMat[3][2] = 0.f - lightpos[Z] * groundplane[W];

  shadowMat[X][3] = 0.f - lightpos[W] * groundplane[X];
  shadowMat[1][3] = 0.f - lightpos[W] * groundplane[Y];
  shadowMat[2][3] = 0.f - lightpos[W] * groundplane[Z];
  shadowMat[3][3] = dot - lightpos[W] * groundplane[W];

}








//////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////       TRACKBALL /////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////
/* globals */
static GLuint    tb_lasttime;
static GLfloat   tb_lastposition[3];

static GLfloat   tb_angle = 0.0;
static GLfloat   tb_axis[3];
static GLfloat   tb_transform[4][4];

static GLuint    tb_width;
static GLuint    tb_height;

static float     tb_motion_speed=0.1;

static GLint     tb_button = -1;
static GLboolean tb_animate = GL_FALSE;


/* functions */
void
_tbPointToVector(int x, int y, int width, int height, float v[3])
{
  float d, a;

  /* project x, y onto a hemi-sphere centered within width, height. */
  v[0] = (2.0 * x - width) / width;
  v[1] = (height - 2.0 * y) / height;
  d = sqrt(v[0] * v[0] + v[1] * v[1]);
  v[2] = cos((3.14159265 / 2.0) * ((d < 1.0) ? d : 1.0));
  a = 1.0 / sqrt(v[0] * v[0] + v[1] * v[1] + v[2] * v[2]);
  v[0] *= a;
  v[1] *= a;
  v[2] *= a;
}

void
_tbAnimate(void)
{
  glutPostRedisplay();
}

void
tbInit()
{
  /* put the identity in the trackball transform */
  glPushMatrix();
  glLoadIdentity();
  glGetFloatv(GL_MODELVIEW_MATRIX, (GLfloat *)tb_transform);
  glPopMatrix();
}

void
tbMatrix()
{
  glPushMatrix();
  glLoadIdentity();
  glRotatef(tb_angle, tb_axis[0], tb_axis[1], tb_axis[2]);
  glMultMatrixf((GLfloat *)tb_transform);
  glGetFloatv(GL_MODELVIEW_MATRIX, (GLfloat *)tb_transform);
  glPopMatrix();

  glMultMatrixf((GLfloat *)tb_transform);
}

void
tbReshape(int width, int height)
{
  tb_width  = width;
  tb_height = height;
}

void
tbStartMotion(int x, int y, int button, int time)
{
  tb_button = button;
  tb_lasttime = time;
  _tbPointToVector(x, y, tb_width, tb_height, tb_lastposition);
}

void
tbStopMotion(int button, unsigned time)
{
  tb_button = -1;

  if (time == tb_lasttime && tb_animate) {
    glutIdleFunc(_tbAnimate);
  } else {
    tb_angle = 0.0;
    if (tb_animate)
      glutIdleFunc(0);
  }
}

void
tbMotion(int x, int y)
{
  GLfloat current_position[3], dx, dy, dz;

  if (tb_button == -1)
    return;

  _tbPointToVector(x, y, tb_width, tb_height, current_position);

  /* calculate the angle to rotate by (directly proportional to the
     length of the mouse movement */
  dx = current_position[0] - tb_lastposition[0];
  dy = current_position[1] - tb_lastposition[1];
  dz = current_position[2] - tb_lastposition[2];
  tb_angle = 90.0 * sqrt(dx * dx + dy * dy + dz * dz)*tb_motion_speed;

  /* calculate the axis of rotation (cross product) */
  tb_axis[0] = tb_lastposition[1] * current_position[2] - 
               tb_lastposition[2] * current_position[1];
  tb_axis[1] = tb_lastposition[2] * current_position[0] - 
               tb_lastposition[0] * current_position[2];
  tb_axis[2] = tb_lastposition[0] * current_position[1] - 
               tb_lastposition[1] * current_position[0];

  /* reset for next time */
  tb_lasttime = glutGet(GLUT_ELAPSED_TIME);
  tb_lastposition[0] = current_position[0];
  tb_lastposition[1] = current_position[1];
  tb_lastposition[2] = current_position[2];

  /* remember to draw new position */
  glutPostRedisplay();
}
////////////////////////////////////////////////////////////////////////////////////////////////////






// load a .RAW file as a texture
GLuint LoadTextureRAW( const char * filename, int width, int height, int wrap )
{
  GLuint texture;
//  int width, height;
  BYTE * data;
  FILE * file;

  // open texture data
  file = fopen( filename, "rb" );
  if ( file == NULL ) return 0;

  // allocate buffer

  data =(BYTE*) malloc( width * height * 3 );

  // read texture data
  fread( data, width * height * 3, 1, file );
  fclose( file );

  // allocate a texture name
  glGenTextures( 1, &texture );

  // select our current texture
  glBindTexture( GL_TEXTURE_2D, texture );

  // select modulate to mix texture with color for shading
  glTexEnvf( GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_REPLACE );

  // when texture area is small, bilinear filter the closest MIP map
  glTexParameterf( GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER,
                   GL_LINEAR_MIPMAP_NEAREST );
  // when texture area is large, bilinear filter the first MIP map
  glTexParameterf( GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR );

  // if wrap is true, the texture wraps over at the edges (repeat)
  //       ... false, the texture ends at the edges (clamp)
  glTexParameterf( GL_TEXTURE_2D, GL_TEXTURE_WRAP_S,GL_REPEAT);
                  // wrap ? GL_REPEAT : GL_CLAMP );

  glTexParameterf( GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
                 //  wrap ? GL_REPEAT : GL_CLAMP );

  // build our texture MIP maps
  gluBuild2DMipmaps( GL_TEXTURE_2D, 3, width,
    height, GL_RGB, GL_UNSIGNED_BYTE, data );

  // free buffer
  free( data );

  return texture;

}

void FreeTexture( GLuint texture )
{

  glDeleteTextures( 1, &texture );

}